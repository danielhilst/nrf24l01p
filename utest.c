#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <arpa/inet.h>

#define pexit(s) ({perror(s); exit(EXIT_FAILURE);})

int main(int argc, char **argv)
{
#define BUFLEN 1000
        char buf[BUFLEN];
        int sock, error, clilen, bytes;
        struct sockaddr_in srv;

        if (argc < 4) {
                printf("Usage: %s IFACE SRV_ADDR PORT\n", argv[0]);
                exit(EXIT_FAILURE);
        }

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (sock == -1)
                pexit("socket");

        memset(&srv, 0, sizeof(srv));
        srv.sin_family =  AF_INET;
        srv.sin_port = htons(atoi(argv[3]));
	int status = inet_aton(argv[2], &srv.sin_addr);
	if (!status) /* note: inet_aton return false for error */
		pexit("inet_aton");	

#define HELLO_WORLD_STR "Hello world"
	printf("Sending message to %s\n", inet_ntoa(srv.sin_addr));
	snprintf(buf, BUFLEN, HELLO_WORLD_STR);
	printf("buf: %s\n", buf);
	error = sendto(sock, buf, BUFLEN, 0, (struct sockaddr *)&srv, sizeof(srv));
	if (error < 0)
		perror("sendto");

	close(sock);
        return 0;
}
