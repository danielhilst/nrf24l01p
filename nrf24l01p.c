/* @TODO: Remove this and vim fold markers
 * vim: set foldmethod=marker:ts=8
 */

/* How it works : {{{
 *
 * A word about addresses
 *
 *	We use 4 bits address. This give us up 16 hosts. The many hosts are in
 *	the same network more noise will have that network and worse
 *	performance so I think 16 hosts is fine for one nRF24L network. Also
 *	having more hosts mean having greater address size which means more
 *	data needed at each frame head, which in turn means more overhead.
 *	Besides source and destination address we need information enough to
 *	permit fragmentation of IP packets. This fragmentation doesn't uses the
 *	IP headers fields since the are "too fat" for 32 bytes of payload with
 *	exception of packet id that is used to unique identify the packet. The
 *	requiriments for this protocol are:
 *
 *	(I)   Support for at last 1500 bytes of payloads
 *	(II)  Connectionless
 *	(III) Fragments should be routable
 *
 * 	To support (I) we need 6 bits for fragment offset and a total_size
 * 	of such size too. Then we need to unique identify that packet. I've
 * 	chosed 16 bits for packet id since this match the IP packet id header.
 *
 *
 * So, how pipes are used?
 * 	RX_ADDR_P0 -> World.
 * 	----------
 *
 * 	The "world" baby!
 *
 * 	RX_ADDR_P1
 * 	----------
 *
 * 	OUR IP ADDRESS. This is how the network (read nRF24L01+) save our address.
 * 	The next pipes are the other 5 possibles hosts at this network. Rember that
 * 	we have /29 mask.
 *
 * 	RX_ADDR_P2..P5
 * 	--------------
 *
 * 	struct nrf24l01p_header {
 * 	#if defined(__BIG_ENDIAN_BITFIELD)
 * 		u8 frag_off:6, mf:1, :1;
 * 		u8 total_size:6,     :2;
 * 		u8 dst:4, src:4;
 * 	#elif defined(__LITTLE_ENDIAN_BITFIELD)
 * 		u8 :1, mf:1, frag_off:6;
 * 		u8 :2, total_size:6;
 * 		u8 src:4, dst:4;
 * 	#endif
 * 		__be16 packet_id;
 * 		u8 payload[];
 * 	};
 *
 * Hardware header consistis of some IP header fields: (OUT OF DATE!)
 *
 * 	IP HEADER
 *
 * 	0	4	8	    14	16    19	24		32
 * 	VERSION IHL	DSCP        ECN	TOTAL_LENGHT
 * 	ID				FLAGS OFFSET
 * 	TTL		PROTO		HEADER_CHKSUM
 * 	SOURCE_ADDR
 * 	DESTINATION_ADDR
 * 	OPTIONS (if IHL > 5)
 *
 * 	VERSION: 	IPv4 should use 4. (Not transmitted). Since this only implement IPv4, this is
 * 		 	assumed 4 aways. SIZE: 4 bits.
 *
 * 	IHL:		Internet Header Lenght. This is the size of the header, which
 * 			is the number of 32 bytes in header. SIZE: 4 bits. (Changes for
 * 			each fragment!?)
 *
 * 	DSCP:		Differentiated Services Code Point. The same for all fragments.
 *	 		Used by services that require realtime streaming link VoIP.
 *
 *
 * 	ECN:		Optional. (not implemented)
 *
 * 	TOTAL_LENGHT:	The same for all fragments. The total size of the packet, including
 * 			header size and data, in bytes. The minimum-lenght packet is 20 byte
 * 			(for IPv4, 20 from IP header + 0 of user data).
 *
 * 	ID:		Identification. Changes for each fragment. It idenfifies the framgment
 * 			from the others.
 *
 * 	Flags:		Tree bits field:
 * 			0 	-> 	Reserved. (Not transmitted).
 * 			1	->	DF. Don't Fragment. (Not transmitted) Used to tell driver to not fragment
 * 					this packet. Since we have only 32 bytes of payload, every
 * 					thing uses fragments. If a packet with this bit setted
 * 					reaches the driver, this one should refuse to transmit this
 * 					packet except if the packet can be transmitted in one frame.
 * 					What is ver unlikely to happen.
 * 			2	->	MF: More fragments. This bit MUST be setted (1) if this is not
 * 					the last segment. If it is, then it MUST be cleared (0).
 *
 * 	Offset:		The fragment offset filed is measured in units eight
 * 			octets blocks. Changes for each fragment.  It specifies the offset a
 * 			particular frament relative to the beginning of the original
 * 			unfragmented IP datagram. The frist fragment has an offset of zero.
 * 			This allow a maximum offset if (2^13 -1) * 8 = 65Kb.  SIZE: 13 bits.
 *
 * 	TTL:		Time to live. The same for all fragments. Counter to packet life cycle.
 *
 * 	Protocol:	Protocol field defines the protocol used in the data portion of the IP datagram. The same
 * 			for all fragments. SIZE: 8 bits.
 *
 * 	Header checksum: (Not transmitted since the hardware can checksum frames for us. :), Size: 16bits
 *
 * 	Source Address:	Transmitted. The same for all fragments. Size 32bits.
 *
 * 	Destination Address: (Not transmitted, it can be graspped from Nordic's registers). Size 32bits.
 *
 * 	Opions:		Options are optional.
 *
 * 	Data:		User's payload.
 *
 * 	--
 * 	References:
 *
 * 	https://tools.ietf.org/html/rfc791
 *
 *
 *
 * }}} */

/* Macros definitions {{{  */
#define DRV_NAME "nrf24l01p"
#define DRV_VERSION "0.0.0"
#define NETDEV_NAME "nrf24l"

#define _dbg(fmt, args...) 			\
	pr_debug("[DEBUG] " DRV_NAME 	\
		":%s(%d): " fmt "\n", 		\
		__func__, __LINE__, ##args)
/* }}} Macro definitions */

/* Bitops {{{ */
#define isset(bit, mask) (bit & mask)
#define setbit(v, mask)  (v |= (mask))
#define clearbit(v, mask) (v &= ~(mask))
/* }}} */

/* Commands {{{ */
#define R_REGISTER(addr) (addr)
#define W_REGISTER(addr) ((addr) + 0x20)
#define W_ACK_PAYLOAD(p) (0xa8 | (0x07 & p))  /* write ack payload */

#define R_RX_PAYLOAD 0x61
#define W_TX_PAYLOAD 0xa0
#define FLUSH_TX     0xe1
#define FLUSH_RX     0xe2
#define REUSE_TX_PL  0xe3
#define ACTIVATE     0x50
#define R_RX_PL_WID  0x60
#define W_TX_PAYLOAD_NO_ACK 0xb0
#define NOP_CMD      0xff
/* }}} Commands */

/* Registers Address {{{ */
#define CONFIG_REG      0x00
#define EN_AA_REG       0x01
#define EN_RXADDR_REG   0x02
#define SETUP_AW_REG    0x03
#define SETUP_RETR_REG  0x04
#define RF_CH_REG       0x05
#define RF_SETUP_REG    0x06
#define STATUS_REG      0x07
#define _RX_DR BIT(6)
#define _TX_DS BIT(5)
#define OBSERVE_TX_REG  0x08
#define RPD_REG         0x09
#define RX_ADDR_P0_REG  0x0a
#define RX_ADDR_P1_REG  0x0b
#define RX_ADDR_P2_REG  0x0c
#define RX_ADDR_P3_REG  0x0d
#define RX_ADDR_P4_REG  0x0e
#define RX_ADDR_P5_REG  0x0f
#define TX_ADDR_REG     0x10
#define RX_PW_P0_REG    0x11
#define RX_PW_P1_REG    0x12
#define RX_PW_P2_REG    0x13
#define RX_PW_P3_REG    0x14
#define RX_PW_P4_REG    0x15
#define RX_PW_P5_REG    0x16
#define FIFO_STATUS_REG 0x17
#define DYNPD_REG       0x1c
#define FEATURE_REG     0x1d
/* }}} Registers */

/* Headers {{{ */
#include <linux/if_arp.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/workqueue.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>

#include <asm/byteorder.h>
/* }}} Headers */

/* Types {{{ */
struct nrf24l01p_header {
#if defined(__BIG_ENDIAN_BITFIELD)
	u8 frag_off:6, mf:1, :1;
	u8 total_size:6,     :2;
	u8 dst:3, src:3,     :2;
#elif defined(__LITTLE_ENDIAN_BITFIELD)
	u8 :1, mf:1, frag_off:6;
	u8 :2, total_size:6;
	u8 :2, src:3, dst:3;
#endif
	__be16 packet_id;
	u8 payload[];
};

struct nrf24l01p {
	struct net_device *netdev;
	struct spi_device *spi;
	struct sk_buff *tx_skb;
	struct workqueue_struct *workqueue;
	int ce;
};
/* }}} Types */

/* SPI Helpers {{{ */
static int spi_write_then_write(struct spi_device *dev, void *buf1, size_t len1, void *buf2, size_t len2)
{
	struct spi_transfer t[] = {
		{ .tx_buf = buf1, .len = len1 },
		{ .tx_buf = buf2, .len = len2 },
	};
	struct spi_message m;
	spi_message_init(&m);
	spi_message_add_tail(&t[0], &m);
	spi_message_add_tail(&t[1], &m);
	return spi_sync(dev, &m);
}

static int _spi_write_then_read_async(struct spi_device *dev,
		                      void *txbuf, size_t txsiz,
		                      void *rxbuf, size_t rxsiz,
			              void (*complete)(void *),
			              void *context)
{
	struct spi_transfer t[] = {
		{ .tx_buf = txbuf, .len = txsiz, },
		{ .tx_buf = rxbuf, .len = rxsiz, },
	};
	struct spi_message m;
	spi_message_init(&m);
	m.complete = complete;
	m.context  = context;
	spi_message_add_tail(&t[0], &m);
	spi_message_add_tail(&t[1], &m);
	return spi_async(dev, &m);
}

static void _spi_write_then_read_worker_complete(void *);
static int  _spi_write_then_read_worker(struct spi_device *dev,
				        void *txbuf, size_t txsiz,
				        void *rxbuf, size_t rxsiz,
				        struct workqueue_struct *wq,
				        struct work_struct *w,
				        gfp_t flags)
{
	BUG_ON(!dev || !wq || !w);
	void **args = kmalloc(flags, sizeof(void *) * 2);
	if (!args)
		return -ENOMEM;
	args[0] = wq;
	args[1] = w;
	return _spi_write_then_read_async(dev,
					txbuf, txsiz,
					rxbuf, rxsiz,
					_spi_write_then_read_worker_complete,
					args);
}

static void _spi_write_then_read_worker_complete(void *arg)
{
	BUG_ON(!arg);
	void **args = arg;
	struct workqueue_struct *wq = args[0];
	struct work_struct *w = args[1];
	queue_work(wq, w);
	kfree(arg);
}

static int _rf_r_cmd(struct nrf24l01p *rf, u8 cmd, u8 *val, size_t len)
{
	BUG_ON(!rf);
	BUG_ON(!rf->spi);
	BUG_ON(!val);
	return spi_write_then_read(rf->spi, &cmd, 1, val, len);
}

static int _rf_r_cmd_worker(struct nrf24l01p *rf, u8 cmd, u8 *val, size_t len, struct workqueue_struct *wq, struct work_struct *w, gfp_t flags)
{
	BUG_ON(!rf);
	BUG_ON(!rf->spi);
	BUG_ON(!val);
	return _spi_write_then_read_worker(rf->spi, &cmd, 1, val, len, wq, w, flags);
}

static int _rf_w_cmd(struct nrf24l01p *rf, u8 cmd, u8 *val, size_t len)
{
	BUG_ON(!rf->spi);
	BUG_ON(!val);
	return spi_write_then_write(rf->spi, &cmd, 1, val, len);
}

static int _rf_cmd(struct nrf24l01p *rf, u8 cmd)
{
	BUG_ON(!rf);
	BUG_ON(!rf->spi);
	return spi_write(rf->spi, &cmd, 1);
}

#define _ce(rf, v) gpio_set_value((rf)->ce, v);
#define _ce_high(rf) _ce(rf, 1)
#define _ce_low(rf)  _ce(rf, 0)
#define _rreg(rf, cmd, val, len)
#define _wreg(rf, cmd, val, len) _rf_w_cmd(rf, W_REGISTER(cmd), val, len)
#define _flush_tx(rf) _rf_cmd(rf, FLUSH_TX)
#define _flush_rx(rf) _rf_cmd(rf, FLUSH_RX)
#define _reuse_tx_pl(rf) _rf_cmd(rf, REUSE_TX_PL)
#define _r_rx_pl_wid(rf, val) _rf_r_cmd(rf, R_RX_PL_WID, val, 1)
#define _w_ack_payload(rf, pipe, pld, len) _rf_w_cmd(rf, W_ACK_PAYLOAD(pipe), pld, len)
#define _w_tx_payload_no_ack(rf, pld, len) _rf_w_cmd(rf, W_TX_PAYLOAD_NO_ACK, pld, len)
/* }}} spi helpers */

/* Nordic helpers {{{ */
static void _reset_to_default(struct nrf24l01p *rf)
{
	_ce_low(rf);
	_wreg(rf, CONFIG_REG, "\x0d", 1); /* crc 2 bytes */
	_wreg(rf, EN_AA_REG, "\x3f", 1); /* auto ack enabled for all pipes */
       	_wreg(rf, SETUP_AW_REG, "\x03", 1);
	_wreg(rf, SETUP_RETR_REG, "\x03", 1); /* auto retry as default */
	_wreg(rf, RF_CH_REG, "\x02", 1);
	_wreg(rf, STATUS_REG, "\x70", 1);
	_wreg(rf, DYNPD_REG, "\x3f", 1); /* enable dynamic payload for all pipes */
	_wreg(rf, FEATURE_REG, "\x03", 1); /* enable all features */
	_flush_tx(rf);
	_flush_rx(rf);
}
/* }}} Noridc helpers */

/* Sysfs {{{ */
ssize_t show_reg(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct spi_device *spi = to_spi_device(dev);
	struct nrf24l01p *rf   = spi_get_drvdata(spi);
	int r; /* register index */
	unsigned int off = 0;
	u8 regs_val[FEATURE_REG + 1][5];
	BUG_ON(!rf);
	BUG_ON(!spi);
	_dbg("");

	for (r = CONFIG_REG; r <= FEATURE_REG; r++) {
		switch (r) {
		case CONFIG_REG ... RPD_REG:
		case RX_PW_P0_REG ... FIFO_STATUS_REG:
		case DYNPD_REG:
		case FEATURE_REG:
		case RX_ADDR_P2_REG ... RX_ADDR_P5_REG:
			_rreg(rf, r, regs_val[r], 1);
			break;
		case RX_ADDR_P0_REG:
		case RX_ADDR_P1_REG:
		case TX_ADDR_REG:
			_rreg(rf, r, regs_val[r], 5);
			break;
		default:
			break;
		}

	}
	for (r = CONFIG_REG; r <= FEATURE_REG; r++) {
		switch (r) {
		case CONFIG_REG ... RPD_REG:
		case RX_PW_P0_REG ... FIFO_STATUS_REG:
		case DYNPD_REG:
		case FEATURE_REG:
		case RX_ADDR_P2_REG ... RX_ADDR_P5_REG:
			off += scnprintf(buf + off,
					PAGE_SIZE - off,
					"%02x:%02x\n",
					r, regs_val[r][0]);
			break;
		case RX_ADDR_P0_REG:
		case RX_ADDR_P1_REG:
		case TX_ADDR_REG:
			off += scnprintf(buf + off,
					PAGE_SIZE - off,
					"%02x:%02x%02x%02x%02x%02x\n",
					r,
					regs_val[r][0],
					regs_val[r][1],
					regs_val[r][2],
					regs_val[r][3],
					regs_val[r][4]);
			break;
		default:
			break;
		}

	}
	return off;
}
static DEVICE_ATTR(registers, 0400, show_reg, NULL);

static struct attribute *_attrs[] = {
	&dev_attr_registers.attr,
	NULL,
};
static struct attribute_group _attr_group = {
	.attrs = _attrs,
};
/* }}} sysfs */

/* IRQ & Worker {{{ */
static u8 _status;
static void nrf24l01p_worker(void *);
static struct work_struct nrf24l01p_work;
static irqreturn_t nrf24l01p_irq(int irq, void *arg)
{
	struct spi_device *spi = arg;
	struct nrf24l01p *rf = spi_get_drvdata(spi);

	BUG_ON(!spi);
	BUG_ON(!rf);

	INIT_WORK(&nrf24l01p_work, (void *)rf);
	_rf_r_cmd_worker(rf,
			STATUS_REG,
			&_status, 1,
			rf->workqueue, &nrf24l01p_work,
			GFP_ATOMIC);

	return IRQ_HANDLED;
}

static void nrf24l01p_worker(void *arg)
{
	struct nrf24l01p *rf = arg;
	if (_status & _TX_DS) {

	}

	if (_status & _RX_DR) {

	}

	_wreg(rf, STATUS_REG, &_status, 1); /* clears IRQ flags at nRF24L01+ */
}
/* }}} irq */

/* net_device stuff {{{ */
static int nrf24l01p_net_open(struct net_device *net)
{
	_dbg("open");
	return 0;
}

static int nrf24l01p_net_stop(struct net_device *net)
{
	_dbg("stop");
	return 0;
}

static int nrf24l01p_start_xmit(struct sk_buff *skb, struct net_device *net)
{
	/* - Get skbuf size.
	 * - Get struct nrfl2401p *rf.
	 * - Init segmentation logic.
	 * - Query skbuf for IP addresses.
	 * - If no IP address can be found notify failure.
	 * - Take the control of nRF24L01+ by pausing rf->workqueue.
	 * - Configure nRF24L01+ addresses registers.
	 * - Transmit each segment with AACK enabled until all.
	 *   segments have been acked or timeout ocurrs.
	 * - Cleanup rf struct if needed.
	 * - Release the control of nRF24L01+ by resuming rf->workqueue.
	 * - How users are notified by socket interface
	 *   about errors on transmission!? & How to handle
	 *   errors at this level?!
	 */
	return 0;
}

static void nrf24l01p_tx_timeout(struct net_device *net)
{
	_dbg("tx_timeout");
}

static const struct net_device_ops nrf24l01p_netdev_ops = {
	.ndo_open 		= nrf24l01p_net_open,
	.ndo_stop 		= nrf24l01p_net_stop,
	.ndo_start_xmit         = nrf24l01p_start_xmit,
	.ndo_tx_timeout         = nrf24l01p_tx_timeout,
};

#define NRF24L01P_ALEN 5
#define NRF24L01P_HLEN 5
#define NRF24L01P_TX_QUEUE_LEN 3
#define NRF24L01P_MTU 1500
static void nrf24l01p_net_init(struct net_device *dev)
{
	_dbg("");
	dev->netdev_ops = &nrf24l01p_netdev_ops;
	dev->mtu             = NRF24L01P_MTU;
	dev->hard_header_len = NRF24L01P_HLEN;
	dev->addr_len        = NRF24L01P_ALEN;
	dev->tx_queue_len    = NRF24L01P_TX_QUEUE_LEN;
	dev->flags          |= IFF_NOARP;
	dev->features       |= NETIF_F_HW_CSUM;
	dev->type           |= ARPHRD_VOID;
#define TX_TIMEOUT (4 * HZ)
	dev->watchdog_timeo = TX_TIMEOUT;
	memset(dev->broadcast, '\xff', NRF24L01P_HLEN);
}
/* }}} net device stuff */

/* driver ops {{{ */
static int nrf24l01p_probe(struct spi_device *spi)
{
	int status = -ENODEV;
	int gpio_ce, gpio_irq;
	struct nrf24l01p*rf = NULL;
	struct device_node *np = spi->dev.of_node;
	struct net_device *net;
	_dbg("");

	if (!np) {
		dev_err(&spi->dev, "no device-tree node");
		return -ENODEV;
	}

	/* device tree stuff */
	gpio_irq = of_get_named_gpio(np, "gpio-irq", 0);
	gpio_ce  = of_get_named_gpio(np, "gpio-ce", 0);
	if (gpio_irq < 0 ||
	    gpio_ce  < 0) {
		dev_err(&spi->dev, "no gpio-ce or gpio-irq in device-tree node");
		return -ENODEV;
	}

	/* requesting irq */
	spi->irq = gpio_to_irq(gpio_irq);
	if (spi->irq < 0) {
		dev_err(&spi->dev, "invalid irq");
		return -ENODEV;
	}

	status = request_irq(spi->irq, nrf24l01p_irq, IRQF_TRIGGER_FALLING, "nRF24L01+", spi);
	if (status) {
		dev_err(&spi->dev, "request_irq failed");
		return -ENODEV;
	}

	/* alloc net_device struct, the initialization goes at init func */
	net = alloc_netdev(sizeof(*rf), NETDEV_NAME "%d", NET_NAME_UNKNOWN, nrf24l01p_net_init);
	if (!net) {
		dev_err(&spi->dev, "no memory");
		goto free_irq;
	}
	SET_NETDEV_DEV(net, &spi->dev);
	rf = netdev_priv(net);
	rf->netdev = net;
	rf->spi = spi;
	net->irq = gpio_irq;
	rf->ce = gpio_ce;
	spi_set_drvdata(spi, rf);
	status = register_netdev(net);
	if (status) {
		dev_err(&spi->dev, "register netdev " DRV_NAME
				" failed %d\n", status);
		goto free_netdev;
	}

	status = sysfs_create_group(&spi->dev.kobj, &_attr_group);
	if (status) {
		dev_err(&spi->dev, "sysfs_create_group failed");
		goto unregister_netdev;
	}

	_reset_to_default(rf); /* setup default settings */

	rf->workqueue = create_singlethread_workqueue("nRF24L01+");
	if (!rf->workqueue)
		goto sysfs_remove;

	return 0;
sysfs_remove:
	sysfs_remove_group(&spi->dev.kobj, &_attr_group);
unregister_netdev:
	unregister_netdev(net);
free_netdev:
	free_netdev(net);
free_irq:
	free_irq(spi->irq, spi);
	_dbg("");
	return status;
}

static int nrf24l01p_remove(struct spi_device *spi)
{
	struct nrf24l01p *rf = spi_get_drvdata(spi);
	_dbg("");

	drain_workqueue(rf->workqueue);
	destroy_workqueue(rf->workqueue);
	sysfs_remove_group(&spi->dev.kobj, &_attr_group);
	free_irq(spi->irq, spi);
	unregister_netdev(rf->netdev);
	free_netdev(rf->netdev);
	return 0;
}

static const struct of_device_id nrf24l01p_of_ids[] = {
        { .compatible = "nrf24l01p" },
        {},
};

static struct spi_driver nrf24l01p_driver = {
	.driver =  {
		.name = "nrf24l01p",
		.owner = THIS_MODULE,
		.of_match_table = nrf24l01p_of_ids,
	},
	.probe = nrf24l01p_probe,
	.remove = nrf24l01p_remove,
};
/* }}} driver ops */

/* module ops {{{ */
int nrf24l01p_init(void)
{
	_dbg("");
	return spi_register_driver(&nrf24l01p_driver);
}

void nrf24l01p_exit(void)
{
	_dbg("");
	spi_unregister_driver(&nrf24l01p_driver);
}
module_init(nrf24l01p_init);
module_exit(nrf24l01p_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Daniel Hilst Selli <danielhilst@gmail.com>");
/* }}} module ops */
