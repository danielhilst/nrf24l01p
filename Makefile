this = $(MAKEFILE_LIST)

ccflags-y := -DDEBUG -Wno-unused-function -Wno-declaration-after-statement
obj-m := nrf24l01p.o

all:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD)
clean:
	$(MAKE) -C $(KERNEL_SRC) M=$(PWD) clean

CFLAGS_$(this) += -Wall -Wextra
utest: utest.c

# Pattern rules
%: %.c
	$(CC) $(CFLAGS) $(LDFLAGS) $(CFLAGS_$(this)) $(LDFLAGS_$(this)) $(CFLAGS_$@) $(LDFLAGS_$@) -o $@ $<

%: %.o
	$(CC) $(LDFLAGS) $(LDFLAGS_$(this)) $(LDFLAGS_$@) -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) $(CFLAGS_$(this)) $(CFLAGS_$@) -c -o $@ $<
